#!/usr/bin/python
# -*- coding: utf-8 -*-
'''test3-jc.py.py

Test script to retrieve parameters frequency from ARES system

Usage: test3-jc.py [-h] [-c CONFIG_FILE] [-f FROM_PID] [-t TO_PID]
                   [-F <date> ] [-T <date> ]

PyArEx package test script

optional arguments:
  -h, --help                               Show this help  message and exit
  -c CONFIG_FILE, --config CONFIG_FILE     Configuration file to use
                                           (default:pyarex.ini)
  -f FROM_PID, --from_pid FROM_PID         Initial parameter identifier
  -t TO_PID, --to_pid TO_PID               Final parameter identifier
  -F Y DOY h m s, --from_date Y DOY h m s  Initial date in the format Y DOY h m s
  -T Y DOY h m s, --to_date Y DOY h m s    Final date in the format Y DOY h m s
  -e SYS_ELEM                              Define system element (default:TM)

Usage example:

  $ python test3-jc.py -c ./cfg.ini -f 1 -t 2 -F 2013 354 0 0 0 -T 2013 354 1 0 0
'''

# make print & unicode backwards compatible
from __future__ import print_function
from __future__ import unicode_literals

import os, sys
_filedir_ = os.path.dirname(__file__)
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

PYTHON2 = False
PY_NAME = "python3"
STRING = str

from astropy.table import Table
from astropy.io import fits

from utime.utime import *

import ares.pyares as pa
#import pandas as pd
import numpy as np
#import fitsio
#import matplotlib.pyplot as plt

import errno
import time
import gzip
import json
import argparse
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


VERSION = '0.0.2'

__author__ = "J C Gonzalez"
__copyright__ = "Copyright 2015-2019, J C Gonzalez"
__license__ = "LGPL 3.0"
__version__ = VERSION
__maintainer__ = "J C Gonzalez"
__email__ = "jcgonzalez@sciops.esa.int"
__status__ = "Development"
#__url__ = ""

# Default configuration
DefaultConfig = os.path.join(os.getenv('PYTHONPATH',
                                       '../../cfg').split(':')[0],
                             'retrieval_config.ini')

# Correspondence ARES Types => FITS BinaryTable Types
Ares2FitsConversion = {
    '1':   'X',   # BIT
    '2':   'B',   # UTINYINT
    '3':   'B',   # STINYINT
    '4':   'I',   # USMALLINT
    '5':   'I',   # SSMALLINT
    '6':   'J',   # UMEDIUMINT
    '7':   'J',   # SMEDIUMINT
    '8':   'K',   # SINT
    '9':   'K',   # UINT
    '10':  'E',   # FLOAT
    '11':  'D',   # DOUBLE
    '12':  'A{}', # STRING
    '13':  '!',   # DATETIME
    '14':  '!',   # JOB
    '15':  '!'    # LOG
}
StringType = 12
DateTimeType = 13

XMLTemplates = {
    'XML': """<?xml version="1.0" encoding="UTF-8"?>
<dpd-le1-hktm:HKTMProduct
    xmlns:pro-le1-vis="http://euclid.esa.org/schema/pro/le1/vis"
    xmlns:bas-imp-stc="http://euclid.esa.org/schema/bas/imp/stc"
    xmlns:sys="http://euclid.esa.org/schema/sys"
    xmlns:bas-utd="http://euclid.esa.org/schema/bas/utd"
    xmlns:bas-imp="http://euclid.esa.org/schema/bas/imp"
    xmlns:pro-le1="http://euclid.esa.org/schema/pro/le1"
    xmlns:bas-imp-fits="http://euclid.esa.org/schema/bas/imp/fits"
    xmlns:dpd-le1-visrawframe="http://euclid.esa.org/schema/dpd/le1/visrawframe"
    xmlns:ins="http://euclid.esa.org/schema/ins"
    xmlns:bas-imp-eso="http://euclid.esa.org/schema/bas/imp/eso"
    xmlns:bas-img="http://euclid.esa.org/schema/bas/img"
    xmlns:bas-ppr="http://euclid.esa.org/schema/bas/ppr"
    xmlns:bas-dtd="http://euclid.esa.org/schema/bas/dtd"
    xmlns:bas-cot="http://euclid.esa.org/schema/bas/cot"
    xmlns:sys-dss="http://euclid.esa.org/schema/sys/dss"
    xmlns:bas-dqc="http://euclid.esa.org/schema/bas/dqc"
    xmlns:bas-fit="http://euclid.esa.org/schema/bas/fit"
    xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <Header>
    <ProductId>{}</ProductId>
    <ProductType>HKTM</ProductType>
    <SoftwareName>HKTMProductGenerator</SoftwareName>
    <SoftwareRelease>2.2</SoftwareRelease>
    <ManualValidationStatus>UNKNOWN</ManualValidationStatus>
    <PipelineRun></PipelineRun>
    <ExitStatusCode>0</ExitStatusCode>
    <DataModelVersion>N</DataModelVersion>
    <MinDataModelVersion>M</MinDataModelVersion>
    <ScientificCustodian>SOC</ScientificCustodian>
    <AccessRights>
      <EuclidConsortiumRead>True</EuclidConsortiumRead>
      <EuclidConsortiumWrite>False</EuclidConsortiumWrite>
      <ScientificGroupRead>False</ScientificGroupRead>
      <ScientificGroupWrite>False</ScientificGroupWrite>
    </AccessRights>
    <Curator>
      <Name>J.C.Gonzalez</Name>
      <Email>JCGonzalez@sciops.esa.int</Email>
    </Curator>
    <Creator>SOC</Creator>
    <CreationDate>{}</CreationDate>
  </Header>
  <Data>
    <DateTimeRange>
{}
    </DateTimeRange>
    <ParameterRange>
{}
    </ParameterRange>
    <ParameterList>
{}
    </ParameterList>
    <HKTMFileList>
{}
    </HKTMFileList>
    <HKTMContainer filestatus="PROPOSED">
{}
    </HKTMContainer>
  </Data>
</dpd-le1-hktm:HKTMProduct>
""",
    'DateTimeRange': '      <FromYDoY>{:04d}.{:03d} {:02d}:{:02d}:{:06.3f}Z</FromYDoY>\n' +
                     '      <ToYDoY>{:04d}.{:03d} {:02d}:{:02d}:{:06.3f}Z</ToYDoY>',
    'PIDRange': '      <FromPID>{}</FromPID>\n      <ToPID>{}</ToPID>',
    'Param': '      <Parameter pid="{}" name="{}" type="{}" prodIndex="{}" hduIndex="{}"/>',
    'Prod': '      <Product index="{}" id="{}" fromPID="{}" toPID="{}">\n{}\n      </Product>',
    'HDU': '        <HDU index="{}" pid="{}" paramName="{}" type="{}"/>',
    'DataCont': '      <FileName>{}</FileName>',
}


#----------------------------------------------------------------------------
# Class: ParamSampleRetriever
#----------------------------------------------------------------------------
class ParamSampleRetriever(object):
    '''
    Simple class to perform an actual data retrieval from the ARES cluster,
    with the help of the PyArEx package.
    '''

    # Framework environment related variables
    Home = os.environ['HOME']
    AresRuntimeDir = ''

    if os.path.isdir(Home + '/ARES_RUNTIME'):
        AresRuntimeDir = Home + '/ARES_RUNTIME'

    if "ARES_RUNTIME" in os.environ:
        # Nominally, the QPFWA env. variable should point to the QPF Working Area
        # main directory (usually /home/eucops/qpf)
        AresRuntimeDir = os.environ["ARES_RUNTIME"]

    # The following hash table shows a series of regular expresions that can be used
    # to deduce the imported data file type
    AresFileTypes = {}
    AresFileTypesCfgFile = "import_file_types.json"

    def __init__(self, cfg_file=None, rqst_mode='pid',
                 from_pid=None, to_pid=None, pids_block=None, names=None,
                 from_date=None, to_date=None, sys_elem='TM',
                 output_dir='./',
                 file_tpl='ares_%F-%T_%f-%t_%YMD1T%hms1-%YMD2T%hms2',
                 file_type='fits'):
        '''
        Instance initialization method
        '''
        # Define config. file if not set in the local environment
        if cfg_file == None:
            if not 'PYAREX_INI_FILE' in os.environ:
                os.environ['PYAREX_INI_FILE'] = cfg_file
        else:
            os.environ['PYAREX_INI_FILE'] = cfg_file

        logger.info('Reading retrieval script config. file {0}'.format(cfg_file))

        self.rqst_mode = rqst_mode

        self.param_names = names

        if self.rqst_mode == 'pid':
            self.from_pid = from_pid
            self.to_pid = to_pid
            self.pid_blk = pids_block
            self.from_pid_blk = self.from_pid
            self.to_pid_blk = self.from_pid_blk + self.pid_blk - 1
            self.name = ''
        else:
            self.from_pid, self.to_pid = (1, 1)
            self.from_pid_blk, self.to_pid_blk, self.pid_blk = (1, 1, 1)
            self.name = 'PARAM'

        logger.info(f'From date {from_date} to date {to_date}')
        if len(from_date) == 6:
            self.year1, self.doy1, self.hour1, self.min1, self.sec1, msec = from_date
        else:
            self.year1, mnth, mday, self.hour1, self.min1, self.sec1, msec = from_date
            self.year1, self.doy1 = ymd_to_ydoy(self.year1, mnth, mday)
        self.timestamp_start = unix_ydoy_to_ms(self.year1, self.doy1, self.hour1, self.min1, self.sec1, msec)
        self.sec1 = self.sec1 + msec * 0.001
        year, self.month1, self.day1 = ydoy_to_ymd(self.year1, self.doy1)

        if len(to_date) == 6:
            self.year2, self.doy2, self.hour2, self.min2, self.sec2, msec = to_date
        else:
            self.year2, mnth, mday, self.hour2, self.min2, self.sec2, msec = to_date
            self.year2, self.doy2 = ymd_to_ydoy(self.year2, mnth, mday)
        self.timestamp_end = unix_ydoy_to_ms(self.year2, self.doy2, self.hour2, self.min2, self.sec2, msec)
        self.sec2 = self.sec2 + msec * 0.001
        year, self.month2, self.day2 = ydoy_to_ymd(self.year2, self.doy2)

        self.outdir = output_dir
        self.file_type = file_type

        self.xmlDateTimeRange = XMLTemplates['DateTimeRange'].format(self.year1, self.doy1,
                                                                     self.hour1, self.min1, self.sec1,
                                                                     self.year2, self.doy2,
                                                                     self.hour2, self.min2, self.sec2)
        self.xmlPIDRange = XMLTemplates['PIDRange'].format(self.from_pid, self.to_pid)

        self.xmlHDUs = []
        self.xmlParams = []
        self.xmlProds = []
        self.xmlCont = []

        self.numberOfSamples = {}

        logger.info('-'*60)
        logger.info("Retrieving samples for parameters with parameter ids in the range {0}:{1}"
                     .format(from_pid, to_pid))
        logger.info("from the date {0}.{1}.{2:02d}:{3:02d}:{4:06.3f}"
                     .format(self.year1, self.doy1, self.hour1, self.min1, self.sec1))
        logger.info("to the date {0}.{1}.{2:02d}:{3:02d}:{4:06.3f}"
                     .format(self.year2, self.doy2, self.hour2, self.min2, self.sec2))
        logger.info("and storing the data in FITS files, in blocks of {0} param.ids"
                     .format(self.pid_blk))
        logger.info('-'*60)

    def run(self):
        '''
        Perform the retrieval of a range of PIDs
        '''

        # Get start time
        start_time = time.time()
        end_time = start_time

        # Initalize the needed datasources.
        # Right now this is hardcoded into the initializer, so for config
        # you need to manage this yourself.
        data_provider = pa.init_param_sampleprovider()
        data_provider.set_system_element_as_any()

        retr_time_total, conv_time_total = (0, 0)

        keep_retrieving = True
        i_pid = self.from_pid
        j_pid = i_pid + self.pid_blk - 1
        param_names_invalid = {}
        gen_files = []
        var_name = ''
        var_type = ''

        nfile = 1

        while keep_retrieving:

            # Set preparation time stamp
            prep_time = time.time()

            # Get parameter names for the range of parameter ids, and retrieve samples as DataFrame
            (param_names, param_syselem) = data_provider.get_parameter_names_from_pids(i_pid, j_pid)
            samples = data_provider.get_parameter_sysel_data_objs(param_names,
                                                                  param_syselem,
                                                                  self.timestamp_start,
                                                                  self.timestamp_end)

            # Set retrieval time stamp
            retr_time = time.time()

            # Currently only FITS files are generated

            # Build initial primary HDU for FITS file
            hdul = self.fits_build_hdr(i_pid, j_pid)

            # Convert sample columns to binary tables
            i = 0
            pid = i_pid

            for column in samples:

                # Loop on samples to add values to the resulting vectors
                time_stamps = []
                values = []
                start = True

                # for s in column:
                #     if start:
                #         var_name = s.get_name()
                #         var_type = s.get_type()
                #         if var_name != param_names[i]:
                #             logger.warning("ERROR: Param. name does not match with expectation!")
                #         logger.info('Generating table {} of {} for PID {} - {} (type={})'
                #                      .format(i + 1, self.pid_blk, pid, var_name, var_type))
                #         start = False
                #
                #     time_stamps.append(s.get_time())
                #
                #     value = s.get_value()
                #     if var_type == DateTimeType:
                #         value = unix_ms_to_datestr(value)
                #
                #     values.append(value)
                #
                #     if var_type == DateTimeType:
                #         var_type = StringType

                i = i + 1
                pid = pid + 1

                if start:
                    param_names_invalid[str(pid - 1)] = param_names[i - 1]
                    continue

                type_conv = Ares2FitsConversion[str(var_type)]
                if var_type == StringType:
                    size_fld = len(max(values, key=len))
                    type_conv = type_conv.format(size_fld if size_fld > 0 else 1)

                self.numberOfSamples[f'pid_{pid - 1:05}'] = (len(column), type_conv)

            # Remove FITS file if exists, and (re)create it
            self.from_pid_blk, self.to_pid_blk = (i_pid, j_pid)

            nfile = nfile + 1
            end_time = time.time()

            retr_time_total = retr_time_total + (retr_time - start_time)
            conv_time_total = conv_time_total + (end_time - retr_time)

            i_pid = j_pid + 1
            j_pid = i_pid + self.pid_blk - 1
            if j_pid > self.to_pid:
                j_pid = self.to_pid

            keep_retrieving = (i_pid < self.to_pid)

        full_time_total = end_time - start_time

        logger.info("Data retrieval:   {:10.3f} s".format(retr_time_total))
        logger.info("Data conversion:  {:10.3f} s".format(conv_time_total))
        logger.info("Total exec. time: {:10.3f} s".format(full_time_total))
        if len(param_names_invalid) > 0:
            logger.info("The following parameters could not be converted:")
            for p in param_names_invalid.keys():
                logger.info('{}: "{}"'.format(p, param_names_invalid[p]))

        return self.numberOfSamples


def configureLogs():
    logger.setLevel(logging.DEBUG)

    # Create handlers
    c_handler = logging.StreamHandler()
    c_handler.setLevel(logging.INFO)

    # Create formatters and add it to handlers
    #c_format = logging.Formatter('%(asctime)s %(levelname).1s %(name)s %(module)s:%(lineno)d %(message)s',
    #                             datefmt='%y-%m-%d %H:%M:%S')
    c_format = logging.Formatter('%(asctime)s %(levelname).1s %(module)s:%(lineno)d %(message)s')
    c_handler.setFormatter(c_format)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    for lname in os.environ['LOGGING_MODULES'].split(':'):
        lgr = logging.getLogger(lname)
        if not lgr.handlers: lgr.addHandler(c_handler)

def getArgs():
    '''
    Parse arguments from command line

    :return: args structure
    '''
    parser = argparse.ArgumentParser(description='ARES Data RetrieverTest script to retrieve data from ARES system',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', dest='config_file', default=DefaultConfig,
                        help='Configuration file to use (default:{})'.format(DefaultConfig))
    parser.add_argument('-f', '--from_pid', dest='from_pid', type=int, default=1,
                        help='Initial parameter identifier')
    parser.add_argument('-t', '--to_pid', dest='to_pid', type=int, default=1000,
                        help='Final parameter identifier')
    parser.add_argument('-F', '--from_date', dest='from_date', type=int, nargs=5,
                        help='Initial date in the format Y DOY h m s')
    parser.add_argument('-T', '--to_date', dest='to_date', type=int, nargs=5,
                        help='Final date in the format Y DOY h m s')
    parser.add_argument('-n', '--num_pids_per_file', dest='num_pids_per_file', type=int,
                        default=100, help='Maximum number of PIDs per file')
    parser.add_argument('-e', '--sys_elem', dest='sys_elem', default='TM',
                        help='Set System Element (default:TM)')

    return parser.parse_args()

def greetings(output_dir, file_list):
    """
    Says hello
    """
    logger.info('='*60)
    logger.info('ares_para_freqs - Retrieves a set of parameters from HMS data base into a FITS file')
    logger.info('='*60)

def main():
    """
    Main program
    """
    configureLogs()

    args = getArgs()

    # Set dates (add ms)
    fromDate = list(args.from_date) + [0]
    toDate = list(args.to_date) + [0]
    rqstm = 'pid'

    # File name template
    filename_tpl = 'EUC_SOC_HKTM_%F-%T_%f-%t_%YMD1T%hms1-%YMD2T%hms2'
    if rqstm == 'name':
        filename_tpl = 'EUC_SOC_HKTM_%F_%N_%YMD1T%hms1-%YMD2T%hms2'

    retriever = ParamSampleRetriever(cfg_file=args.config_file, rqst_mode='pid',
                                     from_pid=args.from_pid, to_pid=args.to_pid,
                                     pids_block=args.num_pids_per_file,
                                     from_date=tuple(fromDate), to_date=tuple(toDate),
                                     output_dir='./', file_tpl=filename_tpl,
                                     sys_elem=args.sys_elem)
    numberOfSamples = retriever.run()

    with open('samples.json', 'w') as fjson:
        json.dump(numberOfSamples, fjson)

    logger.info('Done.')


if __name__ == '__main__':
    main()
